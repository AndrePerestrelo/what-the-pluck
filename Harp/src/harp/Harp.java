package harp;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;
import org.opencv.videoio.VideoCapture;

import harp.ui.HarpController;
import harp.ui.HarpModel;
import harp.ui.HarpView;

public class Harp implements HarpController {

	// 0, 100, 165, 255, 130, 255

	private static final Scalar GREEN = new Scalar(0, 255, 0);
	private static final Scalar RED = new Scalar(0, 0, 255);
	private static final Scalar BLUE = new Scalar(255, 0, 0);

	private static final int TARGET_HEIGHT = 600;
	private static final int TARGET_WIDTH = 800;

	private static final int ROPE_COUNT = 9;

	private volatile boolean playing = true;

	private Mat frame = new Mat();
	private Mat hsvFrame = new Mat();
	private VideoCapture vcap = new VideoCapture();

	private String demoPath = Thread.currentThread().getClass().getResource("/demo.mp4").getPath().substring(1);
	
	private HarpModel model = new HarpModel();
	HarpView view = new HarpView(model, this);

	private DetectMotion detectMotion;

	public static void main(String[] args) throws InterruptedException {

		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		
		String videoLibPath = Thread.currentThread().getClass().getResource("/opencv_ffmpeg330_64.dll").getPath().substring(1);
		System.out.println(videoLibPath);
		System.load(videoLibPath);


		Harp harpController = new Harp();

		harpController.start();

	}

	private void start() throws InterruptedException {

		// 0, 100, 165, 255, 130, 255
		model = new HarpModel();

		model.minHue = 0;
		model.maxHue = 50;
		model.minSaturation = 116;
		model.maxSaturation = 255;
		model.minValue = 175;
		model.maxValue = 255;
		model.showHSV = false;
		model.deviationMin = 6;
		model.frameSkip = 3;
		model.vertexMin = 3;
		model.vertexMax = 10;
		model.areaMin = 50;
		
		view = new HarpView(model, this);

		view.setVisible(true);
		
		vcap.open(demoPath);
		
		detectMotion = new DetectMotion(model);
		detectMotion.init(ROPE_COUNT);
		
		while (true) {
			if (playing) {
				
				boolean frameObtained;
				synchronized(this) {
					frameObtained = vcap.read(frame);
				}
				if (frameObtained) {

//					if (model.referenceXValues == null) {
//						
//						grabReferenceXValues();
//						
//					}

					resize(frame);
					adjustColourSpace();
					Imgproc.erode(hsvFrame, hsvFrame, Imgproc.getStructuringElement(Imgproc.MORPH_ERODE, new Size(5,5)));
					
					Mat outputFrame = new Mat(frame.size(), frame.type());
					if (!model.showHSV) {
						frame.copyTo(outputFrame);
					}

					List<MatOfPoint> contours = detectContours();

					List<MatOfPoint> matchedContours = new LinkedList<>();
					List<MatOfPoint> unmatchedContours = new LinkedList<>();
					classifyContours(contours, matchedContours, unmatchedContours);

					long[] xCoords = getXCoords(matchedContours);
					(new Thread() {
						@Override
						public void run() {
							detectMotion.detect(xCoords);
						
					}}).start();
										

					System.out.println("Offset x-coords (" +xCoords.length+"): " + Arrays.toString(xCoords));

					Imgproc.drawContours(outputFrame, unmatchedContours, -1, RED);

					drawContours(outputFrame, matchedContours, GREEN);
					drawContours(outputFrame, unmatchedContours, RED);

					model.frame = Mat2BufferedImage(outputFrame);
					view.updateFrame();

					Thread.sleep(10);
				} else {
					synchronized(this) {
						if (model.showLive) 
							vcap.open(demoPath);
						else 
							vcap.open(1);
					}
				}

			}
		}
	}

	private void drawContours(Mat outputFrame, List<MatOfPoint> matchedContours, Scalar colour) {
		Imgproc.drawContours(outputFrame, matchedContours, -1, colour);
	}

	private void classifyContours(List<MatOfPoint> contours, List<MatOfPoint> matchedContours,
			List<MatOfPoint> unmatchedContours) {
		for (MatOfPoint contour : contours) {
			
			if (Imgproc.contourArea(contour) < model.areaMin) {
				unmatchedContours.add(contour);
				continue;
			}
			
			MatOfPoint2f c = new MatOfPoint2f(contour.toArray());
			MatOfPoint2f approxContour = new MatOfPoint2f();

			double arcLength = Imgproc.arcLength(c, true);
			Imgproc.approxPolyDP(c, approxContour, 0.04 * arcLength, true);

			int vertexCount = approxContour.toArray().length;
			
			if (vertexCount >= model.vertexMin && vertexCount <= model.vertexMax) {
				matchedContours.add(contour);
			} else {
				unmatchedContours.add(contour);
			}
		}
	}

	private List<MatOfPoint> detectContours() {
		List<MatOfPoint> contours = new LinkedList<>();
		Imgproc.findContours(hsvFrame, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
		return contours;
	}

	private static void resize(Mat frame) {
		double sourceHeight = frame.size().height;
		double sourceWidth = frame.size().width;

		double scaleFactorWidth = TARGET_WIDTH / sourceWidth;
		double scaleFactorHeight = TARGET_HEIGHT / sourceHeight;

		double scaleFactor = scaleFactorWidth > scaleFactorHeight ? scaleFactorHeight : scaleFactorWidth;

		Imgproc.resize(frame, frame, new Size(sourceWidth * scaleFactor, sourceHeight * scaleFactor));
	}

	public static BufferedImage Mat2BufferedImage(Mat m) {
		// Method converts a Mat to a Buffered Image
		int type = BufferedImage.TYPE_BYTE_GRAY;
		if (m.channels() > 1) {
			type = BufferedImage.TYPE_3BYTE_BGR;
		}
		int bufferSize = m.channels() * m.cols() * m.rows();
		byte[] b = new byte[bufferSize];
		m.get(0, 0, b); // get all the pixels
		BufferedImage image = new BufferedImage(m.cols(), m.rows(), type);
		final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
		System.arraycopy(b, 0, targetPixels, 0, b.length);
		return image;
	}

	@Override
	public void play() {
		playing = true;

	}

	@Override
	public void pause() {
		playing = false;

	}

	@Override
	public void colourSpaceAdjusted() {
		if (playing)
			return;
		else {
			adjustColourSpace();
			model.frame = Mat2BufferedImage(hsvFrame);
			view.updateFrame();
		}
	}

	@Override
	public void grabReferenceXValues() {
		resize(frame);
		adjustColourSpace();
		List<MatOfPoint> contours = detectContours();
		List<MatOfPoint> matchedContours = new LinkedList<>();
		List<MatOfPoint> unmatchedContours = new LinkedList<>();
		classifyContours(contours, matchedContours, unmatchedContours);
		model.referenceXValues = getXCoords(matchedContours);
		
//		System.out.println("Reference x-coords (" +model.referenceXValues.length+"): " + Arrays.toString(model.referenceXValues));

	}

	private long[] getXCoords(List<MatOfPoint> contours) {

		long[] xValues = new long[contours.size()];

		int i = 0;
		for (MatOfPoint c : contours) {
			Moments m = Imgproc.moments(c);
			xValues[i] = Math.round(m.get_m10() / m.get_m00());
			i++;
		}
		Arrays.sort(xValues);
		return xValues;

	}

	private void adjustColourSpace() {
		Imgproc.cvtColor(frame, hsvFrame, Imgproc.COLOR_BGR2HSV);
		Core.inRange(hsvFrame, new Scalar(model.minHue, model.minSaturation, model.minValue),
				new Scalar(model.maxHue, model.maxSaturation, model.maxValue), hsvFrame);
	}

	@Override
	public void showLive(boolean b) {
		pause();
		model.showLive = b;
		if (b) {
			
			vcap.open(1);
		}
		else {
			vcap.open(demoPath);
		}
		play();
	}

	@Override
	public void minAreaAdjusted() {
		
	}
	
}
