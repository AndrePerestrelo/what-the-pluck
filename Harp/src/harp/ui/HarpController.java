package harp.ui;

public interface HarpController {
	
	void colourSpaceAdjusted();
	
	void play();
	
	void pause();
	
	void grabReferenceXValues();
	
	void showLive(boolean b);

	void minAreaAdjusted();

}


