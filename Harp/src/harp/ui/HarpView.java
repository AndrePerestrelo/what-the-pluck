package harp.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeListener;

public class HarpView extends JFrame {
	
	private static final long serialVersionUID = -4698110443056657463L;
	private int videoHeight = 600;
	private int videoWidth = 800;
	
	private JPanel contentPane;
	private JLabel videoDisplay;
	
	private HarpModel model;
	
	private HarpController controls;
	
	public HarpView(HarpModel cSpaceModel, HarpController controls) {
		super();
		
		this.model = cSpaceModel;
		this.controls = controls;
		
		contentPane = new JPanel(new BorderLayout());
		this.setContentPane(contentPane);
		
		JPanel center = new JPanel(new FlowLayout(FlowLayout.CENTER));
		center.add(getVideoDisplay());
		center.add(getSliders());
		contentPane.add(center, BorderLayout.CENTER);
		
		contentPane.add(getVideoControls(), BorderLayout.SOUTH);
		
		contentPane.add(getDetectionControls(), BorderLayout.NORTH);
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.pack();
		
	}

	public void updateFrame() {
		ImageIcon image = new ImageIcon(model.frame);
        videoDisplay.setIcon(image);
        videoDisplay.repaint();
	}
	
	private Component getVideoDisplay() {
		videoDisplay = new JLabel();
		videoDisplay.setPreferredSize(new Dimension(videoWidth, videoHeight));
		return videoDisplay;
	}
	
	private Component getSliders() {
		JPanel hsvControls = new JPanel();
		hsvControls.setLayout(new BoxLayout(hsvControls, BoxLayout.Y_AXIS));
		
		hsvControls.add(makeSlider("Min Hue", 0, 179, e ->  { model.minHue = ((JSlider)e.getSource()).getValue(); controls.colourSpaceAdjusted();}, model.minHue));
		hsvControls.add(makeSlider("Max Hue", 0, 179, e -> { model.maxHue = ((JSlider)e.getSource()).getValue(); controls.colourSpaceAdjusted();}, model.maxHue));
		
		hsvControls.add(new JSeparator());
		
		hsvControls.add(makeSlider("Min Saturation", 0, 255, e -> { model.minSaturation = ((JSlider)e.getSource()).getValue(); controls.colourSpaceAdjusted();}, model.minSaturation));
		hsvControls.add(makeSlider("Max Saturation", 0, 255, e -> { model.maxSaturation = ((JSlider)e.getSource()).getValue(); controls.colourSpaceAdjusted();}, model.maxSaturation));
		
		hsvControls.add(new JSeparator());
		
		hsvControls.add(makeSlider("Min Value", 0, 255, e -> { model.minValue = ((JSlider)e.getSource()).getValue(); controls.colourSpaceAdjusted();}, model.minValue));
		hsvControls.add(makeSlider("Max Value", 0, 255, e -> { model.maxValue = ((JSlider)e.getSource()).getValue(); controls.colourSpaceAdjusted();}, model.maxValue));
	
		JCheckBox showHSV = new JCheckBox("Display HSV");
		showHSV.setSelected(model.showHSV);
		showHSV.addActionListener(e ->  model.showHSV = ((JCheckBox) e.getSource()).isSelected() );
		
		hsvControls.add(showHSV);
	
		return hsvControls;
	}
	
	private JPanel makeSlider(String label, int minValue, int maxValue, ChangeListener changeListener, int intialValue) {
		JPanel p = new JPanel();
		p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
		
		JPanel labelPanel = new JPanel();
		labelPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		labelPanel.add(new JLabel(label));
				
		final JLabel l = new JLabel();
		labelPanel.add(l);
		
		JSlider s = new JSlider(minValue, maxValue);
		s.setValue(intialValue);
		l.setText("(" + s.getValue() + ")");
		
		s.setPreferredSize(new Dimension(300, 50));
		s.addChangeListener(e -> {
			int value = ((JSlider)e.getSource()).getValue();
			l.setText("(" + value + ")");
		});
		s.addChangeListener(changeListener);
			
		s.setMinorTickSpacing(10);
		s.setMajorTickSpacing(50);
		s.setPaintTicks(true);
		s.setPaintLabels(true);
		
		p.add(labelPanel);
		p.add(s);
		
		return p;
		
	}
	
	private Component getVideoControls() {
		JPanel videoControls = new JPanel();
		
		JButton playButton = new JButton("Play");
		playButton.addActionListener(e -> controls.play());
		videoControls.add(playButton);
		
		JButton pauseButton = new JButton("Pause");
		pauseButton.addActionListener(e ->  controls.pause());
		videoControls.add(pauseButton);
		
		JButton grabRefButton = new JButton("Grab reference");
		grabRefButton.addActionListener(e -> controls.grabReferenceXValues());
		videoControls.add(grabRefButton);
		
		JCheckBox showLive = new JCheckBox("Live view");
		showLive.setSelected(model.showLive);
		showLive.addActionListener(e ->  {model.showLive = ((JCheckBox) e.getSource()).isSelected(); controls.showLive(model.showLive); } );
		videoControls.add(showLive);
		
		return videoControls;
	}
	
	private Component getDetectionControls() {
		
		final int xSize = 50;
		
		JPanel detectionControls = new JPanel();
		
		detectionControls.add(new JLabel("Vertex min"));
		JTextField tf = new JTextField();
		tf.setText("" + model.vertexMin);
		tf.setPreferredSize(new Dimension(xSize, 20));
		tf.addFocusListener( new FocusListener() {

			@Override
			public void focusGained(FocusEvent e) {
				return;
			}

			@Override
			public void focusLost(FocusEvent e) {
				JTextField source = (JTextField) e.getSource();
				try {
					model.vertexMin = Integer.parseInt(source.getText());
				} catch (NumberFormatException ex) {
					source.setText("");
				}
			}
			
		});
		detectionControls.add(tf);
		
		detectionControls.add(new JLabel("Vertex max"));
		tf = new JTextField();
		tf.setText("" + model.vertexMax);
		tf.setPreferredSize(new Dimension(xSize, 20));
		tf.addFocusListener( new FocusListener() {

			@Override
			public void focusGained(FocusEvent e) {
				return;
			}

			@Override
			public void focusLost(FocusEvent e) {
				JTextField source = (JTextField) e.getSource();
				try {
					model.vertexMax = Integer.parseInt(source.getText());
				} catch (NumberFormatException ex) {
					source.setText("");
				}
			}
			
		});
		detectionControls.add(tf);
		
		detectionControls.add(new JLabel("Deviation min"));
		tf = new JTextField();
		tf.setText("" + model.deviationMin);
		tf.setPreferredSize(new Dimension(xSize, 20));
		tf.addFocusListener( new FocusListener() {

			@Override
			public void focusGained(FocusEvent e) {
				return;
			}

			@Override
			public void focusLost(FocusEvent e) {
				JTextField source = (JTextField) e.getSource();
				try {
					model.deviationMin = Integer.parseInt(source.getText());
				} catch (NumberFormatException ex) {
					source.setText("");
				}
			}
			
		});
		detectionControls.add(tf);
		
		detectionControls.add(new JLabel("Frame skip"));
		tf = new JTextField();
		tf.setText("" + model.frameSkip);
		tf.setPreferredSize(new Dimension(xSize, 20));
		tf.addFocusListener( new FocusListener() {

			@Override
			public void focusGained(FocusEvent e) {
				return;
			}

			@Override
			public void focusLost(FocusEvent e) {
				JTextField source = (JTextField) e.getSource();
				try {
					model.frameSkip = Integer.parseInt(source.getText());
				} catch (NumberFormatException ex) {
					source.setText("");
				}
			}
			
		});
		detectionControls.add(tf);
		
		detectionControls.add(
				makeSlider("Area min", 0, 500, e -> { model.areaMin = ((JSlider)e.getSource()).getValue(); controls.minAreaAdjusted();}, model.areaMin) 
				);
		
		return detectionControls;	
		
	}

}
