package harp.ui;

import java.awt.Image;

public class HarpModel {
	public int minHue;
	public int maxHue;
	public int minSaturation;
	public int maxSaturation;
	public int minValue;
	public int maxValue;
	public Image frame;
	
	public boolean showLive = true;
	public boolean showHSV = true;
	
	public long[] referenceXValues;
	
	public int vertexMin;
	public int vertexMax;
	public int deviationMin;
	public int frameSkip;
	public int areaMin;
	
}
