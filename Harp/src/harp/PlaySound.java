package harp;

import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class PlaySound {

	private static final int ROPE_COUNT = 9;
	private String[] NOTES_SCALE = { "c", "d", "e", "f", "g", "a", "b", "c2", "c" };
	private String Extension = "wav";
	private Clip[] _clip = new Clip[ROPE_COUNT];

	public PlaySound() {

	}

	public void init() {
		try {

			for (int i = 0; i < ROPE_COUNT; i++) {
				String file_name = NOTES_SCALE[i] + "." + Extension;
				String bip = Thread.currentThread().getClass().getResource("/" + file_name).getPath().substring(1);

				_clip[i] = AudioSystem.getClip();
				System.out.println(bip);
				AudioInputStream inputStream = AudioSystem
						.getAudioInputStream(this.getClass().getResourceAsStream("/" + file_name));

				_clip[i].open(inputStream);
			}
		} catch (LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedAudioFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void Play(int rope_index) {
//		int max_frame = _clip[rope_index].getFrameLength();
//		int current_frame = _clip[rope_index].getFramePosition();
//		if (current_frame != 0 && current_frame / max_frame * 100 < 80) {
//			return;
//		}
		
		if (_clip[rope_index].isRunning()) {
			_clip[rope_index].stop(); // 34000
		}
		
		_clip[rope_index].setFramePosition(0);
		_clip[rope_index].start();

	}

}
