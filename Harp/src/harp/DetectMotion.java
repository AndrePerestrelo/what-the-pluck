package harp;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import harp.ui.HarpModel;

public class DetectMotion {

	private static int ROPE_COUNT = 0;
	// private static final long DEVIATION_THRESHOLD = 5;

	private HarpModel _model;

	long[] _deviation;

	int _frameHistoryIndex = 0;
	long[][] _frameHistory;
	PlaySound _PlaySound;

	public DetectMotion(HarpModel model) {
		_model = model;

		_PlaySound = new PlaySound();
		_PlaySound.init();
	}

	public void init(int rope_count) throws IndexOutOfBoundsException {

		if (rope_count > 0) {
			ROPE_COUNT = rope_count;

			_deviation = new long[ROPE_COUNT];
			_frameHistory = new long[3][ROPE_COUNT];
		}

	}

	public void detect(long[] ropes_in_motion) {

		if (ROPE_COUNT != ropes_in_motion.length) {
			return;
		}

		if (_frameHistoryIndex < 3) {
			_frameHistory[_frameHistoryIndex] = ropes_in_motion;
			_frameHistoryIndex++;
			return;
		} else { //if(_frameHistoryIndex % 2 == 0) {
			// if(_frameHistoryIndex % _harp_model.frameSkip != 0) {
			// _frameHistoryIndex++;
			// return;
			// }
			_frameHistory[0] = _frameHistory[1];
			_frameHistory[1] = _frameHistory[2];
			_frameHistory[2] = ropes_in_motion;
			
//			DateFormat df = new SimpleDateFormat("hh:mm:ss.SSS");
//			System.out.println("Time Stamp: " + df.format(new Date()));

			for (int i = 0; i < ROPE_COUNT; i++) {
				long deviation_1 = _frameHistory[0][i] - _frameHistory[1][i];
				long deviation_2 = _frameHistory[1][i] - _frameHistory[2][i];

				System.out.println(" Deviation a: " + deviation_1 + " .vs Deviation b: " + deviation_2 + " min deviation: "
						+ _model.deviationMin);
				
				if (deviation_2 > deviation_1 && Math.abs(deviation_2) - Math.abs(deviation_1) > _model.deviationMin) {
					try {
						System.out.println("I should be playing");
						_PlaySound.Play(i);
					} catch (Exception e) {
					}
				}
			}
		}
		_frameHistoryIndex++;
		

	}
}
